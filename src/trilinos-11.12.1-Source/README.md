![Trilinos](http://trilinos.sandia.gov/common/logo_trilinos_moon.png)

[Website](http://trilinos.org/) |
[Documentation](http://trilinos.org/about/documentation/) |
[Mailing List](https://software.sandia.gov/mailman/listinfo/trilinos-users) |
[Packages](http://trilinos.org/packages/)

[![Coverity Scan
Build](https://scan.coverity.com/projects/1680/badge.svg)](https://scan.coverity.com/projects/1680)

The Trilinos Project is an effort to develop algorithms and enabling
technologies within an object-oriented software framework for the solution of
large-scale, complex multi-physics engineering and scientific problems. A
unique design feature of Trilinos is its focus on packages.


### Installation

- See [TrilinosBuildQuickRef.rst](https://github.com/trilinos/trilinos/blob/master/TrilinosBuildQuickRef.rst) for
  information about installing Trilinos with the CMake build system.

- See [README.windows](https://github.com/trilinos/trilinos/blob/master/cmake/tribits/doc/README.windows) for information on
  building Trilinos in MS Windows with MS Visual C++ against CLAPACK.

- [Sample build scripts for various platforms](https://github.com/trilinos/trilinos/blob/master/sampleScripts/)
  can be found in the source tree.
  Please note that these sample scripts are only a starting point for you
  to develop a script that works for your own platform.


### Documentation

- See one or more of the following publications for assistance with
  installing and using Trilinos:

  * Trilinos Overview (a broad overview)
  * Trilinos Tutorial (in-depth tutorial for new users)

- For help with a particular package, see the website and accompanying
  documentation for that package. Links to these can be found down the
  right side of any page on the website and at [the package website](http://trilinos.org/packages/).

- Information on how to extend Trilinos with your own add-on packages in your
  own external repository see
  [TrilinosBuildQuickRef.rst](https://github.com/trilinos/trilinos/blob/master/TrilinosBuildQuickRef.rst)
  and
  [HOWTO.ADD_EXTRA_REPO](https://github.com/trilinos/trilinos/blob/master/cmake/tribits/doc/HOWTO.ADD_EXTRA_REPO)


### Contributing

The GitHub repository is merely a read-only clone. Contributing to Trilinos is
currently only possible with explicit access to the Trilinos servers. Ask on
the mailing list for assistance.


### License

Trilinos is licensed on a per-package basis. Most packages are now under a BSD
license, some are published under the (L)GPL. Details can be taken from the
documentation of each package.
