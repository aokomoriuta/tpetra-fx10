SET(LIB_REQUIRED_DEP_PACKAGES Teuchos)
SET(LIB_OPTIONAL_DEP_PACKAGES ThreadPool KokkosCore KokkosCompat)
SET(TEST_REQUIRED_DEP_PACKAGES)
SET(TEST_OPTIONAL_DEP_PACKAGES)
SET(LIB_REQUIRED_DEP_TPLS)
SET(LIB_OPTIONAL_DEP_TPLS TBB CUDA CUSPARSE Thrust Cusp MKL) # HWLOC)
SET(TEST_REQUIRED_DEP_TPLS)
SET(TEST_OPTIONAL_DEP_TPLS)
