SET(LIB_REQUIRED_DEP_PACKAGES)
SET(LIB_OPTIONAL_DEP_PACKAGES
  EpetraExt Isorropia Amesos AztecOO Belos Ifpack ML NOX Zoltan STKClassic Stokhos)
SET(TEST_REQUIRED_DEP_PACKAGES)
SET(TEST_OPTIONAL_DEP_PACKAGES Amesos Isorropia Epetra EpetraExt Ifpack Intrepid Pamgen AztecOO ML Zoltan STKClassic Teko Tpetra MueLu KokkosCore KokkosLinAlg KokkosCompat KokkosContainers KokkosMpiComm Stokhos)
SET(LIB_REQUIRED_DEP_TPLS)
SET(LIB_OPTIONAL_DEP_TPLS)
SET(TEST_REQUIRED_DEP_TPLS)
SET(TEST_OPTIONAL_DEP_TPLS)
