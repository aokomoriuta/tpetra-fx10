/*! \mainpage Trilinos/Zoltan2: Load Balancing and Combinatorial Scientific Computing.

\section mainpage_contents Contents
<ul>
<li> \ref z2_main_intro
<li> \ref z2_main_overview
<li> \ref z2_main_examples_partitioning
<li> \ref z2_main_building
</ul>


<hr>

Information specifically for developers can be found at \ref z2_developer.

<hr>

\section z2_main_intro Introduction to Zoltan2

Zoltan2 is a package for partitioning, load balancing and 
combinatorial scientific computing. It can be viewed as
a complete redesign/refactoring of the well known
<a href=http://www.cs.sandia.gov/Zoltan > Zoltan </a>
library into C++ in order to support template programming,
to scale to larger problems, and to support architecture aware partitioning.
This library is currently under active development.
The capabilities in Zoltan2 roughly fall within these areas:
<ul>
<li> Partitioning and load-balancing
<li> Ordering and permutations
<li> Graph coloring 
</ul>

Partitioning functionality that is available now is:
<ul>
<li> \ref blockPage for simple weighted identifiers (really created as a sample algorithm).
<li> \ref mjPage Multi-jagged for weighted geometric coordinates.
<li> \ref rcbPage for weighted geometric coordinates.
<li> \ref scotchPartPage for undirected weighted graphs.
</ul>
These algorithms are all parallel.

Ordering methods currently available:
<ul>
<li> \ref amdPage for fill-reducing ordering.
<li> random ordering
<li> \ref rcmPage for bandwidth-reducing ordering.
<li> sorted_degree ordering
</ul>
These are all serial.

Graph coloring currently supported:
<ul>
<li> Distance-1 vertex coloring
</ul>
Currently serial; multithreaded coloring is planned for a future release.

<hr>

\section z2_main_overview Overview of the library usage

\subsection z2_main_overview_partitioning Basic partitioning

Here we describe the typical user interaction with Zoltan2.  We use partitioning as an example.

1. Create a Zoltan2::InputAdapter object for your data.  This adapter provides a uniform interface to user data for the Zoltan2 library.  Adapter interfaces exist for the following classes of data:

<ul>
<li> Zoltan2::MatrixInput (a row oriented matrix with optional row weights)
<li> Zoltan2::GraphInput (an undirected weighted graph)
<li> Zoltan2::VectorInput (a single or multi-vector with optional weights, also used for geometric coordinates)
<li> Zoltan2::IdentifierInput (simple collection of identifiers)
</ul>

Zoltan2::MeshInput will be available soon.

2. Create a Teuchos::ParameterList with your Zoltan2 parameters.  If you are using a third party library (PT-Scotch, ParMetis) you can include a sublist of parameters for this library.  See \ref z2_parameters for a detailed list of parameterse.

3. Create a Zoltan2::PartitioningProblem.  It is templated on your Zoltan2::InputAdapter type.  The constructor arguments are typically your input adapter and your parameter list.

4. Call Zoltan2::PartitioningProblem::solve().

5. Obtain a Zoltan2::PartitioningSolution object from the Zoltan2::PartitioningProblem.

\subsection z2_main_overview_metrics Obtaining partition quality 

\subsection z2_main_overview_repartitioning Repartitioning

<hr>

\section z2_main_examples_partitioning Partitioning examples

Examples include the following:

- Block partitioning (block.cpp)
- RCB partitioning for a C++ novice (rcb_C.cpp)

<hr>

\section z2_main_building Building Zoltan2

Zoltan2 is part of the Trilinos framework and requires several of the other Trilinos libraries to build.  

Zoltan2 configuration refers to these CMake directives:

  \li \c Trilinos_ENABLE_OpenMP if enabled, use OpenMP for multithreaded execution. (Note not all of Zoltan2 is thread-safe.)
  \li \c Trilinos_ENABLE_Zoltan if enabled Zoltan2 will build a test that compares the performance of Zoltan1 and Zoltan2
  \li \c Zoltan2_ENABLE_Experimental if enabled, code which is still under development will be available for use
  \li \c Explicit Instantiation if explicit instantiation is on, all Zoltan2 tests will be compiled with the instantiated types
  \li \c Zoltan2_ENABLE_Scotch for graph partitioning using the Scotch package
  \li \c Zoltan2_ENABLE_OVIS
  \li \c Zoltan2_ENABLE_AMD

These are some of the compilation flags used by Zoltan2:

  \li \c Z2_OMIT_ALL_STATUS_MESSAGES The \ref debug_level parameter controls the verbosity of status messages.  When compiled with this option, the checks for \ref debug_level are bypassed and all status output code is ignored.
  \li \c Z2_OMIT_ALL_PROFILING Checks are done at runtime to determine whether any of the memory or timer parameters were set, prior to checking memory in use or to start or stop a ttimer.  When compiled with this option, those checks are bypassed.
  \li \c Z2_OMIT_ALL_ERROR_CHECKING  The \ref error_check_level parameter controls the amount of error checking done at runtime.  When this flag is set, all error checking code is compiled out.


*/
