// @HEADER
//
// ***********************************************************************
//
//        MueLu: A package for multigrid based preconditioning
//                  Copyright 2012 Sandia Corporation
//
// Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
// the U.S. Government retains certain rights in this software.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the Corporation nor the names of the
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY SANDIA CORPORATION "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL SANDIA CORPORATION OR THE
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Questions? Contact
//                    Jonathan Hu       (jhu@sandia.gov)
//                    Andrey Prokopenko (aprokop@sandia.gov)
//                    Ray Tuminaro      (rstumin@sandia.gov)
//
// ***********************************************************************
//
// @HEADER
// @HEADER
// @HEADER

#ifndef MUELU_DOXYGEN_DOCUMENTATION_HPP
#define MUELU_DOXYGEN_DOCUMENTATION_HPP

/*!

\mainpage

@image html muelu2.png

\section muelu_index Table of Contents

- \ref muelu_overview
- \ref muelu_authors
- \ref muelu_copyright
- \ref muelu_questions

\section muelu_overview Overview

MueLu is a flexible and extensible multigrid solver framework.  It is intended for the research and
development of new multigrid preconditioning techniques.

\section muelu_authors Authors and Contributors

  - Jeremie Gaidamour, Sandia National Labs
  - Axel Gerstenberger, Rolls Royce
  - Jonathan Hu, Sandia National Labs
  - Andrey Prokopenko, Sandia National Labs
  - Chris Siefert, Sandia National Labs
  - Ray Tuminaro, Sandia National Labs
  - Tobias Wiesner, Technical University Munich

\section muelu_copyright Copyright and License

\verbinclude COPYRIGHT_AND_LICENSE

\section muelu_questions For All Questions and Comments...

   Please contact Jonathan Hu (jhu@sandia.gov), Andrey Prokopenko (aprokop@sandia.gov),
   or Ray Tuminaro (rstumin@sandia.gov).

*/

/* ************************************************************************ */
/* ************************************************************************ */

/*! \page user_guide Users Guide

\section user_guide_index Index

- \ref user_guide_getting_started
- \ref questions

\section user_guide_getting_started Getting Started

*/

/* ************************************************************************ */
/* ************************************************************************ */

/*! \page todo ToDo List

Short Term:
<ul>
<li> ???
</ul>

Long Term:
<ul>
<li> ???
<li>
</ul>

*/

/* ************************************************************************ */
/* ************************************************************************ */

/*! \page faq Frequently Asked Questions

\section Questions
\ref faq1

\ref faq2

\section faq1 1. Why name the code MueLu?
Why not?

\section faq2 2. Put your question here?
Answer your question here.

*/

/* ************************************************************************ */
/* ************************************************************************ */

#endif //ifndef MUELU_DOXYGEN_DOCUMENTATION_HPP
